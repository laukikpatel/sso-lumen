<?php
return [

    'sso_server_app_key' => env('SSO_SERVER_APP_KEY', 'base64:54wACC9botVG3Z+0R86m8JPhHZVZ6JgP0c6+q/pUIqQ='),

    'sso_server_uri' => env('SSO_SERVER_URI', 'http://accounts.gg.dev/'),

    'sso_server_session_domain' => env('SSO_SERVER_SESSION_DOMAIN', '.globalgarner.com'),

    'sso_server_cookies_name' => env('SSO_SERVER_COOKIES_NAME', 'accounts'),

];