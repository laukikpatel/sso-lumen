<?php
namespace Laukikpatel\SSO;

use Config;
use Illuminate\Support\Str;
use Illuminate\Encryption\Encrypter;
use Illuminate\Support\Facades\Redis;
use GuzzleHttp\Client as GuzzleHttpClient;
use GuzzleHttp\Cookie\CookieJar as GuzzleHttpCookie;

class Auth
{

    protected static $data = false;

    protected static $scope = null;

    /**
     * Prefetch User Data
     *
     * @return array|bool|mixed
     */
    public static function init() {

        try {
            if(static::$data) {
                return static::$data;
            }

            $data = [];

            $cookies_name = config('sso.sso_server_cookies_name', null);

            if(!$cookies_name) {
                throw new \Exception("SSO_SERVER_COOKIES_NAME not defined in .env");
            }

            if(isset($_COOKIE[$cookies_name]) && $_COOKIE[$cookies_name]) {

                $encryptDecryptKey = config('sso.sso_server_app_key', null);

                if(!$encryptDecryptKey) {
                    throw new \Exception("SSO_SERVER_APP_KEY not defined in .env");
                }

                if (Str::startsWith($encryptDecryptKey, 'base64:')) {
                    $encryptDecryptKey = base64_decode(substr($encryptDecryptKey, 7));
                }

                $newEncrypter = new Encrypter($encryptDecryptKey, 'AES-256-CBC');
                $accountCookie = $newEncrypter->decrypt( $_COOKIE[$cookies_name] );

                $user = Redis::get('laravel:'.$accountCookie);
                $data = unserialize(unserialize($user));
                static::$data = $data;
            }

            return $data;

        } catch (\Exception $e) {
            info($e->getMessage());
            abort(500, $e->getMessage());
        }

    }

    /**
     * Check user is logged in or not
     *
     * @return bool
     */
    public static function isLoggedIn()
    {
        $sessionData = static::init();
        return (isset($sessionData['logged_in']) && $sessionData['logged_in']) ? true : false;
    }

    /**
     * Get Logged User Data
     *
     * @return User|null
     */
    public static function user()
    {
        $sessionData = static::init();
        return new User((isset($sessionData['user']) && $sessionData['user']) ? (array) $sessionData['user'] : []);
    }

    public static function setUserData(array $data) {
        static::$data = $data;
    }

    /**
     *
     * Set scopes received from access token
     *
     * @param $scopes
     */
    public static function setScopes($scopes){
        static::$scope = explode(',', $scopes);
    }

    /**
     *
     * Get scopes received from access token
     *
     * @return null
     */
    public static function getScopes(){
        return static::$scope;
    }

    /**
     * Get Login page URI
     *
     * @return string
     */
    public static function loginURI()
    {
        return route('ssoLogin');
    }

    /**
     * Get Logout page URI
     *
     * @return string
     */
    public static function logoutURI()
    {
        return route('ssoLogout');
    }


    /**
     * Make a POST Request with SSO Cookies
     *
     * @param string $url
     * @param array $data
     * @param array $options
     * @return \Psr\Http\Message\ResponseInterface
     */
    public static function post( $url, array $data = [], array $options = [] )
    {
        $data = ['form_params' => $data] + $options;
        return static::request('POST', $url, $data);
    }

    /**
     * Make a GET Request with SSO Cookies
     *
     * @param $url
     * @param array $options
     * @return \Psr\Http\Message\ResponseInterface
     */
    public static function get( $url, array $options = [] )
    {
        return static::request('GET', $url, $options);
    }

    /**
     * Make a PUT Request with SSO Cookies
     *
     * @param string $url
     * @param array $data
     * @param array $options
     * @return \Psr\Http\Message\ResponseInterface
     */
    public static function put( $url, array $data = [], array $options = [] )
    {
        $data = ['form_params' => $data] + $options;
        return static::request('PUT', $url, $data);
    }

    /**
     * Make a DELETE Request with SSO Cookies
     *
     * @param string $url
     * @param array $data
     * @param array $options
     * @return \Psr\Http\Message\ResponseInterface
     */
    public static function delete( $url, array $data = [], array $options = [] )
    {
        $data = ['form_params' => $data] + $options;
        return static::request('DELETE', $url, $data);
    }

    /**
     * Make a Request
     *
     * @param $method
     * @param $url
     * @param array $options
     * @return \Psr\Http\Message\ResponseInterface
     */
    public static function request( $method, $url, array $options = [] )
    {
        $cookies_name = config('sso.sso_server_cookies_name', null);
        $cookies_domain = config('sso.sso_server_session_domain', null);

        if($cookies_name && $cookies_domain && isset($_COOKIE[$cookies_name])) {
            $jar = GuzzleHttpCookie::fromArray([$cookies_name => $_COOKIE[$cookies_name]], $cookies_domain);
            $options['cookies'] = $jar;
        }

        $authorization = request()->header('Authorization');
        if($authorization && !isset($options['headers']['Authorization'])) {
            $options['headers']['Authorization'] = $authorization;
        }

        $http = new GuzzleHttpClient;
        return $http->request( $method, $url, $options );
    }

}