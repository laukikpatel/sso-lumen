<?php

namespace Laukikpatel\SSO\Middleware;

use Closure;
use Laukikpatel\SSO\Auth;
use GuzzleHttp\Client;

class SSOMiddleware
{

    protected $allowedScopes;

    protected $userScopes;

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::isLoggedIn()) {

            // Agent Scope Cookie Validate
            $validScope = array_slice(func_get_args(), 2);

            if ($validScope && in_array('agent', $validScope)) {

                $accessTokenCookie = $request->cookie('auth_token');
                if (null !== $accessTokenCookie) {

                    $accessTokenData = null;

                    try {
                        $accessTokenData = json_decode(app('encrypter')->decrypt($accessTokenCookie, false));
                        if (is_null($accessTokenData)) {
                            $accessTokenData = json_decode(app('encrypter')->decrypt($accessTokenCookie, true));
                        }
                    } catch (\Exception $e) {
                        try {
                            $accessTokenData = json_decode(app('encrypter')->decrypt($accessTokenCookie, true));
                        } catch (\Exception $e) {
                            $this->UnAuthorize(401, 'Forbidden - Cookie Error', $e);
                        }
                        $this->UnAuthorize(401, 'Forbidden - Cookie Error', $e);
                    } catch (\Throwable $e) {
                        $this->UnAuthorize(401, 'Forbidden - Cookie Error', $e);
                    }

                    $this->allowedScopes = array_slice(func_get_args(), 2);
                    if ($accessTokenData && isset($accessTokenData['user']) && isset($accessTokenData['user']['agent_details'])) {
                        $this->userScopes = $accessTokenData['scope'] ? explode(',', $accessTokenData['scope']) : [];
                        if (!$this->canAllow()) {
                            $this->UnAuthorize(401, 'Forbidden');
                        }
                    } else {
                        $this->UnAuthorize(401, 'Forbidden');
                    }
                }
//                Disable cookie validation temp
//                else {
//                    $this->UnAuthorize(401, 'Forbidden');
//                }
            }

            return $next($request)
                ->header('Access-Control-Allow-Origin', '*')
                ->header('Access-Control-Allow-Credentials', 'true');
        }

        $authorization = $request->header('Authorization');
        $sso_server_uri = trim(config('sso.sso_server_uri', null), '/');

        if ($authorization && $sso_server_uri) {

            try {
                $client = new Client();

                $res = $client->request(
                    'POST',
                    $sso_server_uri . '/oauth/validate',
                    [
                        'form_params' => [
                            'access_token' => $authorization
                        ]
                    ]
                );

                if ($res->getStatusCode() == 200) {
                    $user = json_decode($res->getBody()->getContents(), true);

                    $this->allowedScopes = array_slice(func_get_args(), 2);
                    $this->userScopes = $user['scope'] ? explode(',', $user['scope']) : [];

                    if (!$this->canAllow()) {
                        throw new \Exception('Forbidden', 403);
                    }

                    if (isset($user['user']) && $user['user']) {
                        $user['logged_in'] = true;
                        Auth::setUserData($user);
                    }
                    return $next($request)
                        ->header('Access-Control-Allow-Origin', '*')
                        ->header('Access-Control-Allow-Credentials', 'true');
                }

            } catch (\Exception $e) {

            }

        }

        //=============================================
        $accessTokenCookie = $request->cookie('auth_token');

        if (null !== $accessTokenCookie) {

//            $accessTokenData = json_decode(decrypt($accessTokenCookie));

            try {
                $accessTokenData = json_decode(app('encrypter')->decrypt($accessTokenCookie, false));
                if (is_null($accessTokenData)) {
                    $accessTokenData = json_decode(app('encrypter')->decrypt($accessTokenCookie, true));
                }
            } catch (\Exception $e) {
                try {
                    $accessTokenData = json_decode(app('encrypter')->decrypt($accessTokenCookie, true));
                } catch (\Exception $e) {
                    $this->UnAuthorize(401, 'Forbidden - Cookie Error', $e);
                }
                $this->UnAuthorize(401, 'Forbidden - Cookie Error', $e);
            } catch (\Throwable $e) {
                $this->UnAuthorize(401, 'Forbidden - Cookie Error', $e);
            }

            if (!$accessTokenData && !isset($accessTokenData->access_token)) {
                $this->UnAuthorize(401, 'Forbidden');
            }

            try {
                $client = new Client();
                $res = $client->request(
                    'POST',
                    $sso_server_uri . '/oauth/validate',
                    [
                        'form_params' => [
                            'access_token' => $accessTokenData->access_token
                        ]
                    ]
                );

                if ($res->getStatusCode() == 200) {

                    $user = json_decode($res->getBody()->getContents(), true);
                    $this->allowedScopes = array_slice(func_get_args(), 2);
                    $this->userScopes = $user['scope'] ? explode(',', $user['scope']) : [];

                    if (!$this->canAllow()) {
                        throw new \Exception('Forbidden', 403);
                    }

                    // Store the scopes received form access token
                    if (isset($user['scope']) && $user['scope']) {

                        Auth::setScopes($user['scope']);

                    }

                    if (isset($user['user']) && $user['user']) {
                        $user['logged_in'] = true;
                        $user['user']['scope'] = $this->userScopes;
                        Auth::setUserData($user);
                    }

                    return $next($request);
                }

            } catch (\Exception $e) {
                \Log::error($e);
                return response()->json(['error' => $e->getMessage(), 'code' => $e->getCode()], $e->getCode());
            }

        }


        //=============================================

        return response()->json(['error' => 'Unauthorized'], 401);

    }

    protected function canAllow()
    {
        if (empty($this->allowedScopes) || in_array('*', $this->userScopes)) {
            return true;
        }

        if (empty($this->userScopes)) {
            return false;
        }

        $isAllowed = array_intersect($this->allowedScopes, $this->userScopes);
        return !empty($isAllowed);
    }

    private function UnAuthorize($StatusCode = 401, $msg, \Exception $previous = null, $headers = [])
    {
        throw new \Symfony\Component\HttpKernel\Exception\HttpException($StatusCode, $msg, $previous, $headers);
    }
}
