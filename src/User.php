<?php
namespace Laukikpatel\SSO;


class User
{

    protected $data;

    public function __construct( $user )
    {
        if(!empty($user)) {
            foreach($user as $key => $val) {
                $this->data[$key] = $val;
            }
        }
    }

    public function __get($key)
    {
        return isset($this->data[$key]) ? $this->data[$key] : null;
    }

    public function toArray()
    {
        return (array) $this->data;
    }

    public function __toString(  )
    {
        return json_encode($this->data);
    }

}