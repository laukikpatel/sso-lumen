<?php
namespace Laukikpatel\SSO;

use Illuminate\Support\ServiceProvider;

class SSOServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     * @internal param Router $router
     */
    public function boot()
    {
        $this->mergeConfigFrom(
            __DIR__.'/config/sso.php', 'sso'
        );

        $this->app->routeMiddleware([
            'sso' => Middleware\SSOMiddleware::class,
        ]);

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

    }
}