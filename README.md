# SSO #

### How do I get set up? ###

Add bellow lines in your composer.json file to register sso repository for composer

    "repositories": [
        {
            "type": "vcs",
            "url": "https://laukikpatel@bitbucket.org/laukikpatel/sso-lumen.git"
        }
    ],

After adding above lines run bellow command to install sso package

    composer require laukikpatel/sso-lumen
    composer dump-autoload
    
After installation completed add bellow line in your bootstrap/app.php to register provider

    $app->register(Illuminate\Redis\RedisServiceProvider::class);
    $app->register(Laukikpatel\SSO\SSOServiceProvider::class);

### Environment variable ###

Add bellow lines in your .env file

    SSO_SERVER_APP_KEY=base64:54wACC9botVG3Z+0R86m8JPhHZVZ6JgP0c6+q/pUIqQ=
    SSO_SERVER_URI=https://accounts.globalgarner.com/
    SSO_SERVER_SESSION_DOMAIN=.globalgarner.com
    SSO_SERVER_COOKIES_NAME=accounts
    
    REDIS_HOST=127.0.0.1
    REDIS_PASSWORD=null
    REDIS_PORT=6379

### Middleware ###

Use middleware for check Authentication

**Example:**

    $app->group(['middleware' => 'sso'], function () use ($app) {
    
        $app->get('/test', function () {
            return Laukikpatel\SSO\Auth::user();
        });
    
    });


### Helper ###

    Check User is logged In or not
    Laukikpatel\SSO\Auth::isLoggedIn()
    
    Get Logged User All Data
    Laukikpatel\SSO\Auth::user()

    Get Logged User UserID
    Laukikpatel\SSO\Auth::user()->user_id
    
    Make POST Request with SSO
    Laukikpatel\SSO\Auth::post($url, ['field_name' => 'value'])
    
    Make GET Request with SSO
    Laukikpatel\SSO\Auth::get($url)
    
    Make PUT Request with SSO
    Laukikpatel\SSO\Auth::put($url, ['key1' => 'value1'])
    
    Make DELETE Request with SSO
    Laukikpatel\SSO\Auth::delete($url, ['key1' => 'value1'])

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact